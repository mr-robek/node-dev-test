import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';

@Injectable()
export class DateStringPipe implements PipeTransform<string, Date> {
    transform(value: string): Date {
        // TODO: add validation for Feb-31
        if (isNaN(Date.parse(value)))
            throw new BadRequestException('Bad Date');
        return new Date(value);
    }
}
