import { Test } from '@nestjs/testing';
import ProductRanking from './ProductRanking';
import { OrderModule } from '../../Order/OrderModule';
import { IBestSellers } from '../Model/IReports';

const createBestSeller = (bestseller: IBestSellers): IBestSellers => ({
    productName: bestseller.productName,
    quantity: bestseller.quantity,
    totalPrice: bestseller.totalPrice
})

describe('ProductRanking Service', () => {
    let productRanking: ProductRanking;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [OrderModule],
            providers: [ProductRanking]
        }).compile();

        productRanking = module.get<ProductRanking>(ProductRanking);
    });

    it('BestSellers for 2019-08-07', async () => {
        expect(await productRanking.getRankingForDate(new Date("2019-08-07")))
            .toEqual([
                createBestSeller({ productName: "Black sport shoes", quantity: 2, totalPrice: 220 }),
                createBestSeller({ productName: 'Cotton t-shirt XL', quantity: 1, totalPrice: 25.75 })
            ]);
    });
    it('BestSellers for 2000-01-01', async () => {
        expect(await productRanking.getRankingForDate(new Date("2001-01-01")))
            .toEqual([]);
    });
});
