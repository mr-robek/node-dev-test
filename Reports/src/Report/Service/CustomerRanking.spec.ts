import { Test } from '@nestjs/testing';
import CustomerRanking from './CustomerRanking';
import { OrderModule } from '../../Order/OrderModule';
import { IBestBuyers } from '../Model/IReports';
const createBestBuyer = (bestBuyer: IBestBuyers): IBestBuyers => ({
    customerName: bestBuyer.customerName,
    totalPrice: bestBuyer.totalPrice
})

describe('CustomerRanking Service', () => {
    let customerRanking: CustomerRanking;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [OrderModule],
            providers: [CustomerRanking]
        }).compile();

        customerRanking = module.get<CustomerRanking>(CustomerRanking);
    });

    it('BestBuyers for 2019-08-07', async () => {
        expect(await customerRanking.getRankingForDate(new Date("2019-08-07")))
            .toEqual([
                createBestBuyer({ customerName: 'John Doe', totalPrice: 135.75 }),
                createBestBuyer({ customerName: 'Jane Doe', totalPrice: 110 })
            ]);
    });
    it('BestBuyers for 2000-01-01', async () => {
        expect(await customerRanking.getRankingForDate(new Date("2001-01-01")))
            .toEqual([]);
    });
});
