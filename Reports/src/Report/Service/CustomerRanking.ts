import { Injectable, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestBuyers } from '../Model/IReports';
import { Customer } from '../../Order/Model'

const createBestBuyer = (customer: Customer): IBestBuyers => ({
    customerName: `${customer.firstName} ${customer.lastName}`,
    totalPrice: 0
})

@Injectable()
export default class CustomerRanking {
    constructor(private readonly orderMapper: OrderMapper) { }

    async getRankingForDate(date: Date): Promise<IBestBuyers[]> {
        let orders = await this.orderMapper.getMappedOrdersForDate(date);
        let bestBuyersMap: { [id: number]: IBestBuyers } = orders.reduce((acc, order) => {
            let bestBuyer = acc[order.customer.id] || createBestBuyer(order.customer);
            bestBuyer.totalPrice += order.products.reduce( (acc,product) => acc + product.price, 0); // order totalPrice
            acc[order.customer.id] = bestBuyer;
            return acc;
        },{})

        let bestBuyers = Object.values(bestBuyersMap)
        .sort((a:IBestBuyers,b:IBestBuyers) => b.totalPrice - a.totalPrice)

        return bestBuyers;
    }
}
