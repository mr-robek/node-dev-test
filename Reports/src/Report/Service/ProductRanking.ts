import { Injectable, Inject } from '@nestjs/common';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { IBestSellers } from '../Model/IReports';
import { Product } from '../../Order/Model'

const createBestSeller = (product: Product): IBestSellers => ({
    productName: product.name,
    quantity: 0,
    totalPrice: 0
})

@Injectable()
export default class ProductRanking {
    constructor(private readonly orderMapper: OrderMapper) { }

    async getRankingForDate(date: Date): Promise<IBestSellers[]> {
        let orders = await this.orderMapper.getMappedOrdersForDate(date);
        // object { [productID]: IBestSellers }
        let bestSellersMap: { [id: number]: IBestSellers } = orders.reduce((acc, order) => {
            order.products.forEach(product => {
                let bestSeller: IBestSellers = acc[product.id] || createBestSeller(product);
                bestSeller.quantity++;
                bestSeller.totalPrice += product.price;
                acc[product.id] = bestSeller;
            })
            return acc;
        },{})

        let bestSellers = Object.values(bestSellersMap)
        .sort((a:IBestSellers,b:IBestSellers) => b.quantity - a.quantity);

        return bestSellers;
    }
}
