import { Controller, Get, Param } from '@nestjs/common';
import { CustomerRanking, ProductRanking } from '../Service'
import { IBestSellers, IBestBuyers } from '../Model/IReports';
import { DateStringPipe } from '../Pipe/DateStringPipe'

@Controller("/report")
export class ReportController {

    constructor(private readonly customerRanking: CustomerRanking, private readonly productRanking: ProductRanking) { }

    @Get("/customer/:date")
    bestBuyers(@Param("date", DateStringPipe) date: Date): Promise<IBestBuyers[]> {
        return this.customerRanking.getRankingForDate(date);
    }

    @Get("/products/:date")
    bestSellers(@Param("date", DateStringPipe) date: Date): Promise<IBestSellers[]> {
        return this.productRanking.getRankingForDate(date);
    }

}
