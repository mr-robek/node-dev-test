import { Module } from '@nestjs/common';
import { ReportController } from './Controller/ReportController';
import { OrderModule } from '../Order/OrderModule';
import { CustomerRanking, ProductRanking } from './Service';


@Module({
  imports: [OrderModule],
  controllers: [ReportController],
  providers: [CustomerRanking, ProductRanking],
})
export class ReportModule {
}
