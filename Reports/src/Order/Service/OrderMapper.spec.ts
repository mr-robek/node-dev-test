import { Test } from '@nestjs/testing';
import { OrderMapper } from './OrderMapper';
import { Repository } from './Repository';

import { Customer, Product, Order } from '../Model';

const JOHN_DOE = Customer.new({id:1,firstName:"John",lastName:"Doe"});
const JANE_DOE = Customer.new({id:2,firstName:"Jane",lastName:"Doe"});
const BLACK_SHOES = Product.new({id:1,name:"Black sport shoes",price:110});
const TSHIRT = Product.new({id:2,name:"Cotton t-shirt XL",price:25.75});
const JEANS = Product.new({id:3,name:"Blue jeans",price:55.99});
const ALL_ORDERS = [
    Order.new({number:"2019/07/1", customer: JOHN_DOE, createdAt:new Date("2019-08-07T00:00:00.000Z"), products:[BLACK_SHOES, TSHIRT]}),
    Order.new({number:"2019/07/2", customer: JANE_DOE, createdAt:new Date("2019-08-07T00:00:00.000Z"), products: [BLACK_SHOES]}),
    Order.new({number:"2019/08/1", customer: JANE_DOE, createdAt:new Date("2019-08-08T00:00:00.000Z"), products: [BLACK_SHOES]}),
    Order.new({number:"2019/08/2", customer: JOHN_DOE, createdAt: new Date("2019-08-08T00:00:00.000Z"), products: [TSHIRT]}),
    Order.new({number: "2019/08/3", customer: JOHN_DOE, createdAt: new Date("2019-08-08T00:00:00.000Z"), products: [JEANS]})
];

describe('OrderMapper Service', () => {
    let orderMapper: OrderMapper;

    beforeEach(async () => {
        const module = await Test.createTestingModule({
            providers: [Repository, OrderMapper]
        }).compile();

        orderMapper = module.get<OrderMapper>(OrderMapper);

        let repository = module.get<Repository>(Repository);
        jest.spyOn(repository, 'fetchCustomers').mockImplementation(() => require("../Resources/Data/customers"));
        jest.spyOn(repository, 'fetchOrders').mockImplementation(() => require("../Resources/Data/orders"));
        jest.spyOn(repository, 'fetchProducts').mockImplementation(() => require("../Resources/Data/products"));
    });

    it('Get All Mapped Orders', async () => {
        expect(await orderMapper.getMappedOrders()).toEqual(ALL_ORDERS);
    });
    it ('Get Mapperd Orders for 2019-08-07', async() => {
        expect(await orderMapper.getMappedOrdersForDate(new Date("2019-08-07"))).toEqual([ALL_ORDERS[0], ALL_ORDERS[1]]);
    })
    it ('Get Mapperd Orders for 2000-01-01', async() => {
        expect(await orderMapper.getMappedOrdersForDate(new Date("2001-01-01"))).toEqual([]);
    })
});
