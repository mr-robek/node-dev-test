import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Customer, Product, Order } from '../Model';

const isSameDay = (pDate1: Date, pDate2: Date): boolean =>
    pDate1 && pDate2 && new Date(pDate1).setUTCHours(0, 0, 0, 0) === new Date(pDate2).setUTCHours(0, 0, 0, 0);

@Injectable()
export class OrderMapper {
    @Inject() repository: Repository;

    async getMappedOrders(): Promise<Order[]> {
        let [rawCustomers, rawOrders, rawProducts] = await Promise.all([
            this.repository.fetchCustomers(),
            this.repository.fetchOrders(),
            this.repository.fetchProducts()
        ])
        // create customers object:{ customerId: Customer} and products object: { productId: Product }
        let customers = rawCustomers.reduce((acc, element) => ({ ...acc, [element.id]: Customer.new(element) }), {});
        let products = rawProducts.reduce((acc, element) => ({ ...acc, [element.id]: Product.new(element) }), {});
        // create the actual Orders list with correct mapping with Customer and Products
        let orders = rawOrders.map(rawOrder => Order.new({
            number: rawOrder.number,
            customer: customers[rawOrder.customer],
            createdAt: new Date(rawOrder.createdAt),
            products: rawOrder.products.map(productId => products[productId])
        }));
        return orders;
    }

    async getMappedOrdersForDate(date: Date): Promise<Order[]> {
        let mappedOrders = await this.getMappedOrders();
        return mappedOrders.filter(order => isSameDay(order.createdAt, date));
    }
}
