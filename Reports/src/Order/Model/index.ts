import Customer from './Customer';
import Order from './Order';
import Product from './Product';
export { Customer, Order, Product };
