export default class Product {
    constructor(
        readonly id: number,
        readonly name: string,
        readonly price: number
    ) {}
    static new({id, name, price}: Product): Product {
        return new Product(id,name,price);
    }
}
