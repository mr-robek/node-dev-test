export default class Customer {
    constructor(
        readonly id:number,
        readonly firstName:string,
        readonly lastName:string
    ) {}
    static new({id, firstName, lastName}: Customer): Customer {
        return new Customer(id, firstName, lastName)
    }
}
