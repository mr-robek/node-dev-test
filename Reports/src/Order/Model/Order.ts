import Customer from './Customer';
import Product from './Product';

export default class Order {
    constructor(
        readonly number: string,
        readonly customer: Customer,
        readonly createdAt: Date,
        readonly products: Product[]
    ) {}
    static new({number, customer, createdAt, products}: Order): Order {
        return new Order(number, customer, createdAt, products);
    }
}
