import { canSort } from './utils';

// QuickSort
export class ASort {

    private static swap(array: number[], i: number, j: number): number[] {
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        return array;
    }

    private static quicksort(array: number[], low: number, high: number): number[] {
        if (low < high) {
            let p = ASort.partition(array, low, high);
            ASort.quicksort(array, low, p);
            ASort.quicksort(array, p+1, high);
        }
        return array;
    };
    private static partition(array: number[], low: number, high: number): number {
        let pivotIndex = low + Math.floor((high-low)/2);
        let pivot = array[pivotIndex];
        let i = low-1, j = high+1;
        while(true) {
            do {
                i++;
            } while (array[i] < pivot);

            do {
                j--;
            } while (array[j] > pivot);

            if ( i >= j) {
                return j;
            }
            ASort.swap(array, i, j);
        }
    }

    public static sort(array: number[]): number[] {
        if (!canSort(array))
            return array;
        return ASort.quicksort(array, 0, array.length-1);
    }
}
