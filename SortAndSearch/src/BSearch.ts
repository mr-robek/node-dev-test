export class BSearch {

    private _operations: number = 0

    get operations(): number {
        return this._operations;
    }

    public search(sortedArray: number[], target: number): number {
        if (!sortedArray || target === undefined)
            return -1;
            
        this._operations = 0;
        let leftIndex = 0, rightIndex = sortedArray.length - 1;
        while (leftIndex <= rightIndex) {
            this._operations++;
            let middle = Math.floor((leftIndex + rightIndex)/2);
            if (sortedArray[middle] < target) {
                leftIndex = middle + 1;
            } else if (sortedArray[middle] > target) {
                rightIndex = middle - 1;
            } else {
                return middle;
            }
        }
        return -1;
    }
}
