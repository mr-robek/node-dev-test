
export function canSort(array:number[]): boolean {
    return array && array.length >= 2;
}
