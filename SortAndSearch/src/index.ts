import { ASort } from './ASort';
import { BSort } from './BSort';
import { BSearch } from './BSearch';

const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

const aSorted = ASort.sort([...unsorted]);
const bSorted = BSort.sort([...unsorted]);
const jsSorted = [...unsorted].sort((a,b)=>a-b);
console.log("Unsorted:\t", unsorted);
console.log("ASort:\t\t", aSorted);
console.log("BSort:\t\t", bSorted);
console.log("JS Sort:\t", jsSorted);

console.log("search:")
const binarySearch = new BSearch();
const foundElements = elementsToFind.map(e => ({target: e, index: binarySearch.search(jsSorted, e), operations: binarySearch.operations}));
console.log(foundElements.map(e => `target: ${e.target}, index: ${e.index}, array: ${jsSorted}, operations: ${e.operations}`).join('\n'));
