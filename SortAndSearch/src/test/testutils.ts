class SearchTestCase {
    testing: number[];
    expected: number[];
    constructor(testing: number[]) {
        this.testing = testing;
        this.expected = testing === undefined ? testing : [...testing].sort((a,b) => a-b);
    }
}

export function getSearchTestCaseArrays(): object {
    const bigArray = new Array(1000).fill(undefined).map( e => (~~(Math.random()*200))-100);
    const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
    return {
        UNDEFINED_ARRAY: new SearchTestCase(undefined),
        EMPTY_ARRAY: new SearchTestCase([]),
        ONE_ELEMENT: new SearchTestCase([1]),
        TWO_ELEMENTS: new SearchTestCase([4,2]),
        THREE_ELEMENTS: new SearchTestCase([4,2,1]),
        NEGATIVE_VALUES: new SearchTestCase([-5,0,-6,-4,22,-4]),
        ASCENDING_ARRAY: new SearchTestCase([-2,-1,0,1,2]),
        DESCENDING_ARRAY: new SearchTestCase([2,1,0,-1,-2]),
        ALMOST_ALL_EQUAL: new SearchTestCase([2,2,2,1,2,2,2]),
        LARGE_ARRAY: new SearchTestCase(bigArray),
        TEST_ARRAY: new SearchTestCase(unsorted)
    }
}
