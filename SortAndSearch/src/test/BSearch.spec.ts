import { BSearch } from '../BSearch'
import { expect } from 'chai';
import 'mocha';

describe("BSearch", () => {
    let binarySearch = new BSearch();
    it("Undefined Array, Defined Target", () => {
        expect(binarySearch.search(undefined, 1)).to.equal(-1)
    })
    it("Defined Array, Undefined Target", () => {
        expect(binarySearch.search([1,2,3], undefined)).to.equal(-1)
    })
    it("Defined Array of Length 1, Found Defined Target", () => {
        expect(binarySearch.search([2], 2)).to.equal(0)
    })
    it("Defined Array, Not Found Defined Target", () => {
        expect(binarySearch.search([1,2,3], -5)).to.equal(-1)
    })
    it("Defined Array, Found Defined Target", () => {
        expect(binarySearch.search([1,2,3], 3)).to.equal(2)
    })
    it("Empty Array, Defined Target", () => {
        expect(binarySearch.search([], 2)).to.equal(-1)
    })
    it ("Tested Data", () => {
        const unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
        const sorted = unsorted.sort( (a,b) => a-b );

        const elementsToFind = [1, 5, 13, 27, 77];
        let expected = elementsToFind.map( e => sorted.indexOf(e));
        let testing = elementsToFind.map(e => binarySearch.search(sorted, e));
        expect(testing).to.eql(expected);
    })
})
