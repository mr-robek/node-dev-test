import { expect } from 'chai';
import 'mocha';
import { getSearchTestCaseArrays } from './testutils'

export function testSorting(sortFunction, title) {
    describe(title, () => {
        let testCases = getSearchTestCaseArrays()
        Object.keys(testCases).forEach( name => {
            it(name, () => {
                let { testing, expected } = testCases[name];
                expect(sortFunction(testing)).to.eql(expected)
            })
        })
    })
}
