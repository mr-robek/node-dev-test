import { canSort } from './utils';

// MergeSort
export class BSort {

    private static merge(left: number[],right: number[]): number[] {
        let result = [];
        while(left.length && right.length) {
            result.push( left[0] <= right[0] ? left.shift() : right.shift() )
        }
        while(left.length) {
            result.push(left.shift());
        }
        while(right.length) {
            result.push(right.shift());
        }
        return result;
    }

    private static mergesort(array: number[]): number[] {
        if (array.length == 1)
            return array;
        let pivot = array.length/2;
        let left = BSort.mergesort(array.slice(0,pivot));
        let right = BSort.mergesort(array.slice(pivot));
        return BSort.merge(left, right);
    }

    public static sort(array: number[]): number[] {
        if (!canSort(array))
            return array;
        return BSort.mergesort(array);
    }
}
